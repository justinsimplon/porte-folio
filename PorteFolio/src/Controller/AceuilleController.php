<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AceuilleController extends AbstractController
{
    /**
     * @Route("/", name="aceuille")
     */
    public function index()
    {
        return $this->render('aceuille/index.html.twig', [
            'controller_name' => 'AceuilleController',
        ]);
    }
}
